#include <QCoreApplication>
#include <iostream>
#include <time.h> //for use datetime

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    char chr[] = "\n";
    char datestring[64];

    // Create a DateTime: (13/10/2022 12:00)  and initialize a tm struct.
    struct tm st = {.tm_sec = 0,
                    .tm_min = 0,
                    .tm_hour = 12,
                    .tm_mday = 13,
                    .tm_mon = 11 - 1,         //0-based so 10 is November
                    .tm_year = 2022 - 1900 };  // 1900 + or -

    // Create a time_t from a tm struct.
    time_t t = mktime(&st);
    struct tm* tm_local = localtime(&t);

    //Format DateTime to Date: YYYY-MM-DD
    strftime(datestring, 64, "%Y-%m-%d", tm_local); // can use the %F shorthand
    printf("Date 1:");
    printf(chr);
    printf(datestring);
    printf(chr);

    printf(chr);

    //Format DateTime to Date: day of month, month, year in full
    strftime(datestring, 64, "%A %d %B %Y", tm_local);
    printf("Date 2:");
    printf(chr);
    printf(datestring);
    printf(chr);

    return a.exec();
}
