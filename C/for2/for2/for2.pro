#-------------------------------------------------
#
# Project created by QtCreator 2024-03-15T02:39:06
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = for2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
