#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    char chr[] = "\n";

    printf("Convert to double");
    printf(chr);
    printf("-----------------------------------");
    printf(chr);

    int number = 19;

    printf("%f", (float) number);
    printf(chr);

    return a.exec();
}
