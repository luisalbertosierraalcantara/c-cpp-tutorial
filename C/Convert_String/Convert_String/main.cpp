#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    char chr[] = "\n";
    char date[] = "12/03/2012";
    char number[] = "67";
    char decimal[] = "96.89";

    printf("Convert to String");
    printf(chr);
    printf("--------------------------");
    printf(chr);
    printf("String Date: %s", date);
    printf(chr);
    printf("String Number: %s", number);
    printf(chr);
    printf("String Decimal: %s", decimal);
    printf(chr);

    return a.exec();
}
