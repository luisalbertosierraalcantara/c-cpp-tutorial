#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // Automatic conversion: int to float
    float decimal = 5;

    printf("%f", decimal); // 5.000000
    
    return a.exec();
}
