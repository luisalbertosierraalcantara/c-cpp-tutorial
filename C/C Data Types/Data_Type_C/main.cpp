#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int id;
    char name[] = "Luis A. Sierra";
    double percentage = 18.55;
    char chr[] ="\n";

    id = 10;

    printf("Id: %d", id);
    printf(chr);
    printf("Name: %s", name);
    printf(chr);
    printf("Percentage: %lf", percentage);
    printf(chr);
    
    return a.exec();
}
