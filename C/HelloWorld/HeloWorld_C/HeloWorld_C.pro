#-------------------------------------------------
#
# Project created by QtCreator 2024-03-09T00:31:54
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = HeloWorld_C
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
