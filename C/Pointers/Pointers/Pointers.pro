#-------------------------------------------------
#
# Project created by QtCreator 2024-03-10T15:02:48
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Pointers
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
