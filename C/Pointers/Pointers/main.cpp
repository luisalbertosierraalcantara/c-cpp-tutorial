#include <QCoreApplication>
#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    int* b;

    b = (int*) 100;

    //==================================
    //C POITERS
    //==================================
    printf("Memory Value: %d", (int) b);
    printf("\n");
    printf("Memory Address: %d",(int) &b);
    printf("\n");

    //==================================
    //C++ POITERS
    //==================================
    cout << "Memory Value: " << (int) b;
    cout << endl;
    cout << "Memory Address: " << (int)&b;
    cout << endl;

    return a.exec();
}
