#include <QCoreApplication>
#include <time.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    char chr[] = "\n";
    int DayNumber = 0;
    printf("DayName of DateTime");
    printf(chr);
    printf("-----------------------------------");
    printf(chr);

    char datestring[64];

    // Create a DateTime: (13/10/2022 12:00)  and initialize a tm struct.
    struct tm st = {.tm_sec = 0,
                    .tm_min = 0,
                    .tm_hour = 12,
                    .tm_mday = 13,
                    .tm_mon = 11 - 1,         //0-based so 10 is November
                    .tm_year = 2022 - 1900 };  // 1900 + or -

    // Create a time_t from a tm struct.
    time_t t = mktime(&st);
    struct tm* tm_local = localtime(&t);

    DayNumber = tm_local->tm_wday; //(tm_wday) 0-based from Sunday

    if (DayNumber == 0)
    {
        char DayName[] = "Sunday";
        printf("Day: %s ",DayName);
        printf(chr);
    }
    else
    if (DayNumber == 1)
    {
        char DayName[] = "Monday";
        printf("Day: %s ",DayName);
        printf(chr);
    }
    else
    if (DayNumber == 2)
    {
        char DayName[] = "Tuesday";
        printf("Day: %s ",DayName);
        printf(chr);
    }
    else
    if (DayNumber == 3)
    {
        char DayName[] = "Wednesday";
        printf("Day: %s ",DayName);
        printf(chr);
    }
    else
    if (DayNumber == 4)
    {
        char DayName[] = "Thursday";
        printf("Day: %s ",DayName);
        printf(chr);
    }
    else
    if (DayNumber == 5)
    {
        char DayName[] = "Friday";
        printf("Day: %s ",DayName);
        printf(chr);
    }
    else
    if (DayNumber == 6)
    {
        char DayName[] = "Saturday";
        printf("Day: %s ",DayName);
        printf(chr);
    }

    printf(chr);
    printf("Date: %s ",asctime(tm_local));

    return a.exec();
}
