#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    char chr[] = "\n";

    printf("Convert to Integer");
    printf(chr);
    printf("-----------------------------------");
    printf(chr);
    char text = 'A';
    int number = (int) text;
    printf("Int Number: %d", number);
    printf(chr);

    return a.exec();
}
