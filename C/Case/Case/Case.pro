#-------------------------------------------------
#
# Project created by QtCreator 2024-03-14T20:37:23
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Case
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
