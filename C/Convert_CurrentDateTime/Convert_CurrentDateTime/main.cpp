#include <QCoreApplication>
#include <time.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    char chr[] = "\n";

    /* Get current time as time_t which is an integer, usually long.
       This is generally the number of seconds from midnight on 1 Jan 1970. */
    time_t t = time(NULL);

    /* Get tm structs from time_t.tm has members for seconds, minutes,
       hours, day, month, year etc. */

    struct tm* tm_gmt = gmtime(&t);

    printf("Current GMT time is: %s ", asctime(tm_gmt));
    printf(chr);

    struct tm* tm_local = localtime(&t);

    printf("Current local time is: %s ", asctime(tm_local));
    printf(chr);

    return a.exec();
}
