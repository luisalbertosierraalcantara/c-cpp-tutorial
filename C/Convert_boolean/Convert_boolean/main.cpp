#include <QCoreApplication>
#include <stdbool.h> //for use boolean in C99 and later

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    printf("Convert to Boolean");
    printf("\n");
    printf("-----------------------------------");
    printf("\n");

    int status = 0;

    /* Boolean -> String
    1 -> True
    0 -> False */


    bool Boleano = (bool) status;

    if (Boleano)
       printf("Boolean True: %d",  Boleano);
    else
       printf("Boolean False: %d",  Boleano);

    printf("\n");


    return a.exec();
}
