#include <QCoreApplication>
#include <iostream>
#include <QString>
#include <QDate>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QDate date = QDate::currentDate();
    QString number_toString = QString::number(67);
    QString decimal_toString = QString::number(96.89);
    QString boolean_toString = QString::number(true);

    cout << "Convert to String";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;
    cout << "String Date: " << date.toString().toStdString();
    cout << endl;
    cout << "String Number: " << number_toString.toStdString();
    cout << endl;
    cout << "String Decimal: " << decimal_toString.toStdString();
    cout << endl;
    cout << "String Boolean: " << boolean_toString.toStdString();
    cout << endl;
    
    return a.exec();
}
