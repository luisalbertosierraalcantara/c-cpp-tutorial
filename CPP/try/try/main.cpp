#include <QCoreApplication>
#include <iostream>
#include <Qstring>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    try {
      int result = 0;
      int x,y;
      int exception;

      cout << "Enter the First Number: ";
      cout << endl;
      cin >> x;

      cout << "Enter the Second Number: ";
      cout << endl;
      cin >> y;

      if (x != 0 && y != 0)  {
        result = x / y;
        cout << "Result: " << result;
        cout << endl;
      } else {;
        exception = 1;
        throw exception;
      }
    }
    catch (...) {
      cout << "ERROR: Cannot divide by 0";
      cout << endl;
    }
    
    return a.exec();
}
