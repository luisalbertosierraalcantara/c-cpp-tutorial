#-------------------------------------------------
#
# Project created by QtCreator 2024-03-15T15:46:42
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = try
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
