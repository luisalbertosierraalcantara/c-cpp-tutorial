#include <QCoreApplication>
#include <iostream>
#include <time.h>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int DayNumber = 0;
    string DayName = "";

    cout << "DayName of DateTime";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;

    char datestring[64];

    // Create a DateTime: (13/10/2022 12:00)  and initialize a tm struct.
    struct tm st = {.tm_sec = 0,
                    .tm_min = 0,
                    .tm_hour = 12,
                    .tm_mday = 13,
                    .tm_mon = 11 - 1,         //0-based so 10 is November
                    .tm_year = 2022 - 1900 };  // 1900 + or -

    // Create a time_t from a tm struct.
    time_t t = mktime(&st);
    struct tm* tm_local = localtime(&t);

    DayNumber = tm_local->tm_wday; //(tm_wday) 0-based from Sunday

    if (DayNumber == 0)
    {
        DayName = "Sunday";
    }
    else
    if (DayNumber == 1)
    {
        DayName = "Monday";
    }
    else
    if (DayNumber == 2)
    {
        DayName = "Tuesday";
    }
    else
    if (DayNumber == 3)
    {
        DayName = "Wednesday";
    }
    else
    if (DayNumber == 4)
    {
        DayName = "Thursday";
    }
    else
    if (DayNumber == 5)
    {
        DayName = "Friday";
    }
    else
    if (DayNumber == 6)
    {
        DayName = "Saturday";
    }

    cout << "Day: " << DayName;
    cout << endl;
    cout << "Date:  " << asctime(tm_local);
    cout << endl;

    return a.exec();
}
