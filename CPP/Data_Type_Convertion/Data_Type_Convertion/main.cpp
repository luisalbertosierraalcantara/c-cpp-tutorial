#include <QCoreApplication>
#include <iostream>

using namespace  std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int x = 5;

    // Automatic conversion: int(x) to float
    float decimal = x + 14.67;

    cout << decimal; //19.67

    return a.exec();

}
