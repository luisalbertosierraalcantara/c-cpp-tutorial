#include <QCoreApplication>
#include <iostream>

using namespace std;

class Class1 {
  public :
    void GetMSG() {
      cout << "What is your name?";
      cout << endl;
      cout << "..........";
      cout << endl;
    }
};

class Class2  : public Class1 {
  public :
    void GetMSG() {
      cout << "My Name is Luis A. Sierra";
      cout << endl;
      cout << "..........";
      cout << endl;
    }
};

class Class3 : public Class1 {
  public :
    void GetMSG() {
      cout << "So What is your Occupation?";
      cout << endl;
      cout << "..........";
      cout << endl;
    }
};

class Class4 : public Class1 {
  public :
    void GetMSG() {
      cout << "I'm a Computing Systems Engineer.";
      cout << endl;
      cout << "..........";
      cout << endl;
    }
};


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    Class1  cls1;
    Class2  cls2;
    Class3  cls3;
    Class4  cls4;

    cls1.GetMSG();
    cls2.GetMSG();
    cls3.GetMSG();
    cls4.GetMSG();

    return a.exec();
}
