#-------------------------------------------------
#
# Project created by QtCreator 2024-03-15T15:02:48
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = polymorphism
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
