#include <QCoreApplication>
#include <iostream>
#include <QString>
#include <QDateTime>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "DayName of DateTime";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;
    QString text = "13/11/2022 12:00:00";
    QDateTime date = QDateTime::fromString(text,"dd/M/yyyy h:mm:s");

    int DayNumber = date.date().dayOfWeek();

    //get the name of the day in the operating system language
    //QString weekDay = QDate::longDayName(DayNumber);

    string DayName;

    if (DayNumber == 1)
    {
        DayName = "Monday";
    }
    else
    if (DayNumber == 2)
    {
        DayName = "Tuesday";
    }
    else
    if (DayNumber == 3)
    {
        DayName = "Wednesday";
    }
    else
    if (DayNumber == 4)
    {
        DayName = "Thursday";
    }
    else
    if (DayNumber == 5)
    {
        DayName = "Friday";
    }
    else
    if (DayNumber == 6)
    {
        DayName = "Saturday";
    }
    if (DayNumber == 7)
    {
        DayName = "Sunday";
    }

    cout << endl;
    cout << DayName;
    cout << endl;
    //cout << "Date: " << date.toString().toStdString();
    cout << endl;
    
    return a.exec();
}
