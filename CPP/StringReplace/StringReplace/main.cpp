#include <QCoreApplication>
#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    cout << "String Replace";
    cout << endl;
    cout << "-----------------------------";
    cout << endl;

    std::string text = "C++";

    text = text.replace(1,2,"PP");

    cout << "Replace C++ by CPP: " << text;
    cout << endl;

    return a.exec();
}
