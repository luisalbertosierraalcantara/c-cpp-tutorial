#-------------------------------------------------
#
# Project created by QtCreator 2024-03-14T15:47:09
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = StringReplace
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
