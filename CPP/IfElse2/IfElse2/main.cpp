#include <QCoreApplication>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int age = 67;

    if (age < 18) {
       cout <<  "You are young";
    }
    else
    {
       cout <<  "You are older";
    }

    return a.exec();
}
