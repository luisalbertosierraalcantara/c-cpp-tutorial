#-------------------------------------------------
#
# Project created by QtCreator 2024-03-14T20:33:56
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = IfElse2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
