#include <QCoreApplication>
#include <iostream>
#include <sstream>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "Convert to double";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;
    string text = "19.5";
    float number = 0.0;

    std::stringstream string_tofloat;
    string_tofloat << text;
    string_tofloat >> number;

    cout <<  "Double Decimal: " << number;
    cout << endl;
    
    return a.exec();
}
