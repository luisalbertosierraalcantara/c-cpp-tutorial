#include <QCoreApplication>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int i =1;
    bool condition = true;

    while (condition)
    {
        if (i == 50) condition = false;

        cout << "Number: " << i;
        cout << endl;
        i++;
    }


    return a.exec();
}
