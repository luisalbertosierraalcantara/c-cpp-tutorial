#include <QCoreApplication>
#include <iostream>
#include <time.h>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    time_t t = time(NULL);

    struct tm* tm_gmt = gmtime(&t);

    cout << "Current GMT time is: " << asctime(tm_gmt);
    cout << endl;

    struct tm* tm_local = localtime(&t);

    cout <<  "Current local time is: " << asctime(tm_local);
    cout << endl;

    return a.exec();
}
