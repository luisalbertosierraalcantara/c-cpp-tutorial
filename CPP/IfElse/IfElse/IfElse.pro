#-------------------------------------------------
#
# Project created by QtCreator 2024-03-14T20:25:13
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = IfElse
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
