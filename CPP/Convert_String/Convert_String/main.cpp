#include <QCoreApplication>
#include <iostream>
#include <string>  //used for string and to_string()

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "Convert to String";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;
    cout << "String Date: " << "12/03/2021";
    cout << endl;
    cout << "String Number: " << to_string(67);
    cout << endl;
    cout << "String Decimal: " << to_string(96.89);
    cout << endl;
    cout << "String Boolean: " << to_string(true);
    cout << endl;

    return a.exec();
}
