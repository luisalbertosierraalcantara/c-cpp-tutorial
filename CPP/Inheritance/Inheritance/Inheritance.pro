#-------------------------------------------------
#
# Project created by QtCreator 2024-03-15T04:02:51
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Inheritance
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
