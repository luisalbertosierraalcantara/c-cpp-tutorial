#include <QCoreApplication>
#include <iostream>

using namespace std;

class Class1 {
  private:

    int Account;

  public:

    // Set
    void setAccount(int value) {
      Account = value;
    }

    // Get
    int getAccount() {
      return Account;
    }
};

class Class2 : public Class1 { //Inherits Objects from Class1
  public:

    void ShowData()
    {
       setAccount(357886);

       cout << "Account: " << getAccount();
       cout << endl;
       cout << "Name: " << "Luis A. Sierra";
    }

};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Class2 print;

    print.ShowData();
    
    return a.exec();
}
