#include <QCoreApplication>
#include <iostream>
#include <QString>
#include <QDateTime>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "Convert to Current DateTime";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;

    QString datetime = QDateTime::currentDateTime().toString("dd/MM/yyyy h:mm:s ap");

    cout << "DateTime: " << datetime.toStdString();
    cout << endl;

    return a.exec();
}
