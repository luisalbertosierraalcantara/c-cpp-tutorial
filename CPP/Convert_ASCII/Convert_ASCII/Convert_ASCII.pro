#-------------------------------------------------
#
# Project created by QtCreator 2024-03-14T15:22:24
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Convert_ASCII
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
