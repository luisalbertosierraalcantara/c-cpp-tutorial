#include <QCoreApplication>
#include <QString>
#include <QVariant>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "Convert to Boolean";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;

    /* String -> Boolean
    True -> 1;
    False -> 0 */

    QString text= "true";
    bool Boleano = QVariant(text).toBool();

    bool Boleano2 = true;
    QString Boleano_String = QString::number(Boleano2);

    cout << "Boolean Boleano: " <<  Boleano;
    cout << endl;
    cout << "Boolean Boleano2: " <<  Boleano_String.toStdString();
    cout << endl;

    return a.exec();
}
