#include <QCoreApplication>
#include <iostream>
#include <sstream>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "Convert to Boolean";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;
    string text= "1";

    /* Boolean -> String
    1 -> True
    0 -> False */

    std::stringstream Boleano_toString;
    Boleano_toString << text;
    bool Boleano;
    Boleano_toString >> Boleano;

    cout << "Boolean Boleano: " <<  Boleano;
    cout << endl;

    return a.exec();
}
