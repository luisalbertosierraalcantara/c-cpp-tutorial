#include <QCoreApplication>
#include <iostream>
#include <string.h>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int id;
    string name = "Luis A. Sierra";
    float percentage = 18.55;
    char gender = 'M';
    bool isVerified;

    id = 10;
    isVerified = true;

    cout << "Id: " << id;
    cout << endl;
    cout << "Name: " << name;
    cout << endl;
    cout << "Percentage: " << percentage;
    cout << endl;
    cout << "Gender: " << gender;
    cout << endl;
    cout << "Verfied: " << isVerified;
    cout << endl;

    return a.exec();
}
