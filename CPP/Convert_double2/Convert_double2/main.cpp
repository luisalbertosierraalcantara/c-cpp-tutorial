#include <QCoreApplication>
#include <QString>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "Convert to double";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;
    QString text = "19.5";

    float number = text.toFloat();

    QString number_toString = QString::number(number);
    cout << "Double Decimal: " << number_toString.toStdString();
    cout << endl;

    return a.exec();
}
