#-------------------------------------------------
#
# Project created by QtCreator 2024-03-14T15:57:35
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = StringReplace2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
