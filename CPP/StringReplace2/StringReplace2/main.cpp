#include <QCoreApplication>
#include <iostream>
#include <QString>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "String Replace";
    cout << endl;
    cout << "-----------------------------";
    cout << endl;

    QString text = "C++";

    text = text.replace("++","PP");

    cout << "Replace C++ by CPP: " << text.toStdString();
    cout << endl;

    return a.exec();
}
