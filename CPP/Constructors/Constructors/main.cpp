#include <QCoreApplication>
#include <iostream>

using namespace std;

class Class1 {
  public:

    // Constructor
    Class1() {
      cout << "Hello Luis!";
    }

};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    Class1 obj; //this will call the constructor automatically!

    return a.exec();
}
