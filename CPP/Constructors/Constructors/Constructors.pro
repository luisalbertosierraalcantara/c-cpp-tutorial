#-------------------------------------------------
#
# Project created by QtCreator 2024-03-15T03:38:33
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Constructors
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
