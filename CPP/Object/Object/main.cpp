#include <QCoreApplication>
#include <iostream>

using namespace std;

class Class1 {
  public:

     int account;
     string name;

};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Class1 cls;

    cls.account = 1233455;
    cls.name = "Luis A. Sierra";


    cout << "Accout: " << cls.account;
    cout << endl;
    cout << "Name: " << cls.name;

    return a.exec();
}
