#-------------------------------------------------
#
# Project created by QtCreator 2024-03-15T03:23:17
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Object
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
