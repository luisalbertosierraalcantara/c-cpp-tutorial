#include <QCoreApplication>
#include <iostream>
#include <QString>
#include <QTime>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "Convert to Time";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;
    QString text = "12:00:00";

    QTime time = QTime::fromString(text,"h:m:s");

    cout << "Time: " << time.toString().toStdString();
    cout << endl;

    return a.exec();
}
