#include <QCoreApplication>
#include <iostream>

using namespace std;

class Class1 {
  public:

    void ShowData() {
      cout << "Hello Luis!";
    }

};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    Class1 cls;

    cls.ShowData();

    return a.exec();
}
