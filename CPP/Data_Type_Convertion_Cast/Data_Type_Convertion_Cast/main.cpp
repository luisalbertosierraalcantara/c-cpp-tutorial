#include <QCoreApplication>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    double value1 = 114.16;   //double variable
    float value2 = 432.52;    //float variable

    int result = (int) value1 + (int) value2;  //Manually converted to int

    printf("%d", result);
    printf("\n");
    cout << result;
    
    return a.exec();
}
