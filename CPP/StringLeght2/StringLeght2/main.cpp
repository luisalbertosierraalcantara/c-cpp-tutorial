#include <QCoreApplication>
#include <iostream>
#include <QString>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "String Length";
    cout << endl;
    cout << "-----------------------------";
    cout << endl;

    QString text = "C++";

    int length = text.length();

    cout << "Length: " << length;
    cout << endl;

    return a.exec();
}
