#-------------------------------------------------
#
# Project created by QtCreator 2024-03-14T19:40:39
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Arrays
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
