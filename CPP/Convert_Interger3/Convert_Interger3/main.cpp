#include <QCoreApplication>
#include <QString>
#include <iostream>

using namespace std;


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "Convert to Integer";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;
    QString text = "15";

    int number = text.toInt();

    QString number_toString = QString::number(number);

    cout << "Int Number: " << number_toString.toStdString();

    cout << endl;
    
    return a.exec();
}
