#-------------------------------------------------
#
# Project created by QtCreator 2024-03-15T14:12:51
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = multilevel_inheritance
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
