#include <QCoreApplication>
#include <iostream>

using namespace std;

//parent
class Class1 {
  private:
    int Account;

  public:

    void setAccount(int value) {
      Account = value;
    }

    int getAccount() {
      return Account;
    }
};

//child
class Class2 : public Class1 { //Inherits Objects from Class1
  public:
    string name;

    void setName(string value) {
          name = value;
    }

    string getName() {
      return name;
    }

};

//grandchild
class Class3 : public Class2 { //Inherits Objects from Class1 and Class2
  public:
    void ShowData()
    {
       setAccount(357886);
       setName("Luis A. Sierra");

       cout << "Account: " << getAccount();
       cout << endl;
       cout << "Name: " << getName();
    }

};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Class3 print;

    print.ShowData();

    return a.exec();
}
