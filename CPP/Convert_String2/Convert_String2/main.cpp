#include <QCoreApplication>
#include <iostream>
#include <sstream> //for convert to string

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int number = 67;
    std::stringstream number_stream;
    number_stream << number;
    std::string number_toString = number_stream.str();

    float decimal = 96.89;
    std::stringstream decimal_stream;
    decimal_stream << decimal;
    std::string decimal_toString = decimal_stream.str();

    bool boolean = true;
    std::stringstream boolean_stream;
    boolean_stream << boolean;
    std::string boolean_toString = boolean_stream.str();

    cout << "Convert to String";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;
    cout << "String Date: " << "12/03/2021";
    cout << endl;
    cout << "String Number: " << number_toString;
    cout << endl;
    cout << "String Decimal: " << decimal_toString;
    cout << endl;
    cout << "String Boolean: " << boolean_toString;
    cout << endl;
    
    return a.exec();
}
