#include <QCoreApplication>
#include <iostream>
#include <string>


using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "Convert to Integer";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;
    string text = "15";

    int number = stoi(text);
    cout << "Int Number: " << number;
    cout << endl;
    
    return a.exec();
}
