#include <QCoreApplication>
#include <iostream>
#include <QString>
#include <QDate>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "Convert to Date";
    cout << endl;
    cout << "-----------------------------------";
    cout << endl;
    QString text = "13/10/2022";
    QDate date = QDate::fromString(text,"dd/M/yyyy");

    cout << "Date: " << date.toString().toStdString();
    cout << endl;

    return a.exec();
}
