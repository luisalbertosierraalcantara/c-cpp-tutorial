#include <QCoreApplication>
#include <iostream>

using namespace std;

void data()
{
    cout << "Hello World!";
    cout << endl;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    data();

    return a.exec();
}
