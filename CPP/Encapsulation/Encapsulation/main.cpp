#include <QCoreApplication>
#include <iostream>

using namespace std;

class Class1 {
  private:

    int Account;

  public:

    // Set
    void setAccount(int value) {
      Account = value;
    }

    // Get
    int getAccount() {
      return Account;
    }
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Class1 cls;

    cls.setAccount(1233455);

    cout << "Account: " << cls.getAccount();

    
    return a.exec();
}
