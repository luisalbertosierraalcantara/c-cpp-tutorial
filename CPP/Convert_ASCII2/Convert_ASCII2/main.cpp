#include <QCoreApplication>
#include <iostream>
#include <QChar>
#include <QString>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    cout << "Convert to ASCII";
    cout << endl;
    cout << "-----------------------------";
    cout << endl;

    QChar chr = 65;

    QString GetAscii = chr;

    cout << "Convert to ASCII: " << GetAscii.toStdString();
    cout << endl;


    return a.exec();
}
