#include <QCoreApplication>
#include <iostream>

using namespace std;

class Class1 {
  public :
    string Greetings() {
      return "Hello" ;
    }
};

class Class2 {
  public :
    string GetMSG() {
      return "My Name is Luis A. Sierra." ;
    }
};

// Multiple Inheritance
class Class3 : public Class1, public Class2 {
   public :
    void ShowData()
       {
          cout << Greetings();
          cout << endl;
          cout << GetMSG();
       }
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    Class3 print;

    print.ShowData();

    return a.exec();
}
