#-------------------------------------------------
#
# Project created by QtCreator 2024-03-15T14:42:19
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = multiple_inheritance
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
