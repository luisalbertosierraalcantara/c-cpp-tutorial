#-------------------------------------------------
#
# Project created by QtCreator 2024-03-15T02:35:32
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = for
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
