#include <QCoreApplication>
#include <iostream>

using namespace std;

int SUM(int x, int y) {

  return x + y;

}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    cout << "SUM: " << SUM(5,5);

    return a.exec();
}
