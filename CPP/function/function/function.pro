#-------------------------------------------------
#
# Project created by QtCreator 2024-03-15T03:08:43
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = function
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
